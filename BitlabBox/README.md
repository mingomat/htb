# Bitlab HTB
[Box Link](https://www.hackthebox.eu/home/machines/profile/207)

## Use JS function to login to Gitlab Account
* look arround for a login file
* find the `.js` file `Gitlab_Login` under `http://10.10.10.114/help/bookmarks.html`
* deobfusacte the `.js` file here: [jsnice](200~http://www.jsnice.org/)
* login to the gitlab account with found credentials

## Upload reverse shell & get user
* look arround for intresting stuff
* find the profile files [here](http://10.10.10.114/profile/)
* upload a reverse shell `shell.php` by adding a new file to the git repo `http://10.10.10.114/root/profile/blob/master/`. The shell needs to point the attackers ip and port!!!
* open a terminal window and start a local tcp listener e.g. `nc -lvp 4444`
* visit the `php` file under `http://10.10.10.114/profile/shell.php`
* there is now a reverse shell active in your terminal
* start a more interactive shell by passing `python -c 'import pty;pty.spawn("/bin/bash")'`

## privesc to user
* In order to get user priviliges look arround for intresting stuff
* find a piece of code under the snippets tab of the gitlab account
* this code seems to query a profiles database, but it is not outputting the result
* create a new php file called `db.php` insert the found snippet
* add a `for loop` to the `db.php` to echo the results
* upload the `db.php` file the same way as done for the `shell.php`
* visit the `db.php` file on `http://10.10.10.114/profile/db.php`
* find the credentials for the user
* `cd` to the user directory a `cat` user.txt

## privesc to root
* make sure you are still in the open reverse shell to the server
* transfer the file `RemoteConnection.exe` via `nc`. How to is described here: [Link](https://nakkaya.com/2009/04/15/using-netcat-for-file-transfers/)
* run the file with `wine`
* keep the message in mind
* examine the file with a reverse engineering tool like ollydbg
	* search for the message from earlier, set a breakpoint and check the stack!!!
* ssh into the server and `cat` the `root.txt` file
* done!!! 
